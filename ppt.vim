" Vim syntax file
" Language: Using this for beamer stuff
" Maintainer: Not me
" Latest Revision: August 2010

" case insensitive
syntax case ignore

" Keywords
setlocal iskeyword+=.
syntax match pptPeriod /\. /
highlight link pptPeriod DiffChange
syntax keyword pptNotDerogatory print code text perl design bug bugs groff
syntax keyword pptNotDerogatory editor book fanzine notcourier readme
highlight link pptNotDerogatory Type
syntax keyword pptKeyword TeX
highlight link pptKeyword Type
syntax keyword pptDerogatory job jobs boredom random wikipedia
highlight link pptDerogatory Type

" Testing
syntax match pptIdentifier /\<\l\+\>/
" highlight link pptIdentifier Type
syntax match pptComment /#.*/
highlight link pptComment Visual
syntax region pptParens start=/(/ end=/)/
highlight link pptParens WildMenu
syntax region pptQuote start=/"/ end=/"/
highlight link pptQuote WildMenu
syntax region pptAB start=/a/ end=/b/
highlight link pptAB DiffAdd
syntax region pptCD start=/c/ end=/d/
highlight link pptCD DiffChange
